const arrWhithCode = [49, 50, 51, 52, 53, 54, 55, 56, 57, 48, 189, 187, 81, 87, 69, 82, 84, 89, 85, 73, 79,
    80, 219, 221, 65, 83, 68, 70, 71, 72, 74, 75, 76, 186, 222, 220, 90, 88, 67, 86, 66, 78, 77, 188, 190, 191];
    
const Keyboard = {
    elements: {
        main: null,
        keysContainer: null,
        keys: []
    },

    eventHandlers: {
        oninput: null,
        onclose: null
    },

    properties: {
        value: "",
        capsLock: false,
        shift: false
    },

    init() {
        // Create main elements
        this.elements.main = document.createElement("div");
        this.elements.keysContainer = document.createElement("div");

        // Setup main elements
        this.elements.main.classList.add("keyboard", "keyboard--hidden");  //,"keyboard--hidden"
        this.elements.keysContainer.classList.add("keyboard__keys");
        this.elements.keysContainer.appendChild(this._createKeys());

        this.elements.keys = this.elements.keysContainer.querySelectorAll(".keyboard__key");

        // Add to DOM
        this.elements.main.appendChild(this.elements.keysContainer);
        document.body.appendChild(this.elements.main);

        // Automatically use keyboard for elements with .use-keyboard-input
        document.querySelectorAll(".use-keyboard-input").forEach(element => {
            element.addEventListener("focus", () => {
                this.open(element.value, currentValue => {
                    element.value = currentValue;
                });
            });
        });
    },

    _createKeys() {
        const fragment = document.createDocumentFragment();
        const keyLayout = [
            "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "backspace",
            "tab", "q", "w", "e", "r", "t", "y", "u", "i", "o", "p",
            "caps", "a", "s", "d", "f", "g", "h", "j", "k", "l", "enter",
            "shift", "z", "x", "c", "v", "b", "n", "m", ",", ".", "?",
            "done", "space"
        ];
        // Creates HTML for an icon
        const createIconHTML = (icon_name) => {
            return `<i class="material-icons">${icon_name}</i>`;
        };

        keyLayout.forEach(key => {
            const keyElement = document.createElement("button");
            const insertLineBreak = ["backspace", "p", "enter", "?"].indexOf(key) !== -1;
            // Add attributes/classes
            keyElement.setAttribute("type", "button");
            keyElement.classList.add("keyboard__key");
            if (key === "shift") {
                keyElement.setAttribute("id", "shift");
            }
            switch (key) {
                case "backspace":
                    keyElement.setAttribute("data-id", "Backspace");
                    keyElement.classList.add("keyboard__key--wide");
                    keyElement.innerHTML = createIconHTML("backspace");

                    keyElement.addEventListener("click", () => {
                        this.properties.value = this.properties.value.substring(0, this.properties.value.length - 1);
                        this._triggerEvent("oninput");
                    });

                    break;

                case "tab":
                    keyElement.setAttribute("data-id", "Tab");
                    keyElement.classList.add("keyboard__key--wide");
                    keyElement.innerHTML = createIconHTML("keyboard_tab");
                    break;
                case "caps":
                    keyElement.setAttribute("data-id", "CapsLock");
                    keyElement.classList.add("keyboard__key--wide", "keyboard__key--activatable");
                    keyElement.innerHTML = createIconHTML("keyboard_capslock");

                    keyElement.addEventListener("click", () => {
                        this._toggleCapsLock();
                        keyElement.classList.toggle("keyboard__key--active", this.properties.capsLock);
                    });
                    break;

                case "enter":
                    keyElement.setAttribute("data-id", "Enter");
                    keyElement.classList.add("keyboard__key--wide");
                    keyElement.innerHTML = createIconHTML("keyboard_return");
                    keyElement.addEventListener("click", () => {
                        this.properties.value += "\n";
                        this._triggerEvent("oninput");
                    });

                    break;

                case "shift":
                    keyElement.setAttribute("id", "shift");
                    keyElement.setAttribute("data-id", "Shift");
                    keyElement.classList.add("keyboard__key--wide", "keyboard__key--activatable");
                    keyElement.innerHTML = createIconHTML("keyboard_arrow_up");
                    keyElement.addEventListener("click", () => {
                        this._toggleShift();
                        keyElement.classList.toggle("keyboard__key--active");
                    });
                    break;

                case "space":
                    keyElement.setAttribute("data-id", " ");
                    keyElement.classList.add("keyboard__key--extra-wide");
                    keyElement.innerHTML = createIconHTML("space_bar");

                    keyElement.addEventListener("click", () => {
                        this.properties.value += " ";
                        this._triggerEvent("oninput");
                    });
                    break;

                case "done":
                    keyElement.classList.add("keyboard__key--wide", "keyboard__key--dark");
                    keyElement.innerHTML = createIconHTML("keyboard_hide");

                    keyElement.addEventListener("click", () => {
                        this.close();
                        this._triggerEvent("onclose");
                    });

                    break;

                default:
                    keyElement.setAttribute("data-id", key);
                        keyElement.textContent = key.toLowerCase();
                    keyElement.addEventListener("click", () => {
                        if (this.properties.capsLock && this.properties.shift) {
                            this.properties.value += key.toLowerCase()
                            this._toggleShift();
                            document.getElementById("shift").classList.remove("keyboard__key--active");
                        } else if (this.properties.capsLock && !this.properties.shift) {
                            this.properties.value += key.toUpperCase()
                        } else if (!this.properties.capsLock && this.properties.shift) {
                            this.properties.value += key.toUpperCase()
                            this._toggleShift();
                            document.getElementById("shift").classList.remove("keyboard__key--active");
                        } else {
                            this.properties.value += key.toLowerCase()
                        }
                        this._triggerEvent("oninput");
                    });
                    break;
            }

            fragment.appendChild(keyElement);

            if (insertLineBreak) {
                fragment.appendChild(document.createElement("br"));
            }
        });

        return fragment;
    },

    _triggerEvent(handlerName) {
        if (typeof this.eventHandlers[handlerName] == "function") {
            this.eventHandlers[handlerName](this.properties.value);
        }
    },

    _toggleCapsLock() {
        // edit this by getModifierState() method 
        // console.log(Keyboard.properties.capsLock);
        this.properties.capsLock = !this.properties.capsLock;
        for (const key of this.elements.keys) {
            if (key.childElementCount === 0) {
                key.textContent = this.properties.capsLock ? key.textContent.toUpperCase() : key.textContent.toLowerCase();
            }
        }
    },

    _toggleShift() {
        this.properties.shift = !this.properties.shift;
        for (const key of this.elements.keys) {
            if (key.childElementCount === 0) {
                if (this.properties.shift && this.properties.capsLock) {
                    key.textContent = key.textContent.toLowerCase();
                } else if (!this.properties.shift && this.properties.capsLock) {
                    key.textContent = key.textContent.toUpperCase();
                } else if (this.properties.shift) {
                    key.textContent = key.textContent.toUpperCase();
                } else {
                    key.textContent = key.textContent.toLowerCase();
                }
            }
        }
    },

    open(initialValue, oninput, onclose) {

        this.properties.value = initialValue || "";
        this.eventHandlers.oninput = oninput;
        this.eventHandlers.onclose = onclose;
        this.elements.main.classList.remove("keyboard--hidden");
    },

    close() {
        this.properties.value = "";
        this.eventHandlers.oninput = oninput;
        this.eventHandlers.onclose = onclose;
        this.elements.main.classList.add("keyboard--hidden");
    },

    _keyAnimation() {
        window.addEventListener("keydown", (event) => {
            const keyBoardPressBTN = event.key;
            Keyboard.elements.keys.forEach(element => {
                if (element.dataset.id === keyBoardPressBTN) {
                    element.classList.add("keyboard__key__active")
                }
            })

        });

        window.addEventListener("keyup", (event) => {
            const keyBoardPressBTN = event.key;
            Keyboard.elements.keys.forEach(element => {
                if (element.dataset.id === keyBoardPressBTN) {
                    element.classList.remove("keyboard__key__active")
                }
            })

        });
    }
};

window.addEventListener("DOMContentLoaded", function () {
    Keyboard.init();
    Keyboard._keyAnimation();

});

window.addEventListener("keydown", (event) => {
    if (event.key === "Shift" && event.repeat === false) {
        Keyboard._toggleShift();
        let a = document.getElementsByClassName("keyboard__key--wide");
        for (let i = 0; i < a.length; i++) {
            if (a[i].dataset.id === event.key) {
                a[i].classList.toggle("keyboard__key--active");
            }
        }
    } else if
        (event.key === "CapsLock" && event.repeat === false) {
        Keyboard._toggleCapsLock();
        let a = document.getElementsByClassName("keyboard__key--wide");
        for (let i = 0; i < a.length; i++) {
            if (a[i].dataset.id === event.key) {
                a[i].classList.toggle("keyboard__key--active");
            }
        }
    } else if
        (event.key === "Backspace") {
        Keyboard.properties.value = Keyboard.properties.value.substring(0, Keyboard.properties.value.length - 1);
    } else if
        (event.key === " ") {
        Keyboard.properties.value += " ";
    } else if
        (event.key === "Enter") {
        Keyboard.properties.value += "\n";
    }

    for (let i = 0; i < arrWhithCode.length; i++) {
        if (event.keyCode === arrWhithCode[i]) {
            Keyboard.properties.value += event.key
        }
    }
});

window.addEventListener("keyup", (event) => {
    if (event.key === "Shift") {
        Keyboard._toggleShift();
        let a = document.getElementsByClassName("keyboard__key--wide");
        for (let i = 0; i < a.length; i++) {
            if (a[i].dataset.id === event.key) {
                a[i].classList.remove("keyboard__key--active");
            }
        }
    }
});

