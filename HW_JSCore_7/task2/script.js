const BackgroundItem = {

    elements: {

        container: null,
        keysContainer: null,
        buttons: []
    },

    images: {

        first: "https://img.wallpapic.com.ua/i2926-721-55/medium/voda-priroda-hvili-more-shpalery.jpg",
        second: "https://i.pinimg.com/564x/6f/da/b8/6fdab8292c720b064414dc3f6037b130.jpg",
        third: "https://www.teahub.io/photos/full/38-383275_-.jpg",
        fourth: "https://i.pinimg.com/564x/54/8d/9c/548d9c504285cb33a0d88c143273e333.jpg",
        fifth: "https://webmandry.com.ua/wp-content/uploads/2018/12/novogodnie-oboi-2019-11.jpg",
        sixth: "https://wallpapers-fenix.eu/full/180624/000020532.jpg",
        seventh: "https://torange.biz/photo/36/HD/desktop-wallpaper-36485.jpg",
        eighth: "https://torange.biz/photo/38/HD/desktop-wallpaper-screensaver-spring-38322.jpg",
        ninth: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTeAGSWFMxJGHIIZ2vHzoYlgsXeWVxNrOomTg&usqp=CAU"

    },

    initCollor() {
        if(document.querySelector(".background-option")){
        document.querySelector(".background-option").remove();
    }

        const collors = ["steelblue", "brown", "coral", "darkseagreen", "green", "red", "blue", "blueviolet", "yellow",];
        this.elements.container = document.createElement("div");
        this.elements.container.classList.add("background-option");
        document.body.append(this.elements.container);
        for (let i = 0; i < 9; i++) {
            let element = document.createElement("div");
            element.classList.add(`background-option__key`);
            element.classList.add(`background-option__key--color--${collors[i]}`);
            element.addEventListener("click", () => {
                document.body.removeAttribute("style")
                document.body.style.backgroundColor = `${collors[i]}`;
            })
            this.elements.container.appendChild(element)
        }
    },

    innitImages() {
        if(document.querySelector(".background-option")){
            document.querySelector(".background-option").remove();
        }
        const arrWhithImages = Object.keys(this.images);
        this.elements.container = document.createElement("div");
        this.elements.container.classList.add("background-option");
        document.body.appendChild(this.elements.container);
        for (let i = 0; i < 9; i++) {
            let element = document.createElement("div");
            element.classList.add(`background-option__key`);
            element.classList.add(`background-option__key--image--${arrWhithImages[i]}`);
            element.addEventListener("click", () => {
                document.body.removeAttribute("style")
                document.body.style.backgroundImage = `url(${this.images[arrWhithImages[i]]})`;
            })
            this.elements.container.appendChild(element)
        }

    }



}

document.getElementById("collor").addEventListener("click", () => {
    BackgroundItem.initCollor();
})

document.getElementById("image").addEventListener("click", () => {
    BackgroundItem. innitImages();
})

