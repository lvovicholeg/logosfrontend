const button = document.getElementById(`button1`)
const task = document.getElementsByName(`text`)
const list = document.getElementById('list')
const error_close = document.getElementsByClassName('error_close')
const error = document.getElementsByClassName('error')
const warningText = document.getElementsByClassName('warning_text')

function addToList(someTask) {
    list.innerHTML += `<dt><input type="checkbox">
    <label>${someTask}</label><br></dt>`;
    task[0].value = '';
}

list.addEventListener('click', function deleteFromList(event) {
    let elemToDelete = event.target;
    let dttag = document.getElementsByTagName('dt')
    if (list.children.length === 1) {
        warningText[1].innerHTML = 'Останній таск у списку Ви не можна вилучити!';
        error[0].style.display = 'block'
    } else {
        elemToDelete.parentElement.remove();
    }
})

button.addEventListener('click', function (event) {
    if (task[0].value === '') {
        warningText[1].innerHTML = 'Пусте поле не можна добавити!';
        error[0].style.display = 'block'
    }
    else {
        addToList(task[0].value)
    }
})

error_close[0].addEventListener('click', function closeError(event) {
    error[0].style.display = 'none'
})