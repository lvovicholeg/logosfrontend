//variables
const getS = selector => document.querySelector(selector);
const colors = ['red', 'green', 'blue', 'brown', 'yellow', 'black', 'pink', 'purple', 'lightgray']
//end of variables

//function which takes content from the top block and compiles it to the html-code in the bottom block
getS('.btn-edit').onclick = function () {
    getS('.edit-block').classList.add('display-block');
    getS('.style-block').classList.remove('display-block');
    getS('.edit-area').value = getS('.top-block').innerHTML;
}
// end of function

// function which saves and adds all edited code in the bottom block to the top-block 
getS('.btn-save').onclick = function () {
    getS('.edit-block').classList.remove('display-block');
    getS('.top-block').innerHTML = getS('.edit-area').value;
}
// end of function

// function which opens block with styles
getS('.btn-style').onclick = function () {
    getS('.style-block').classList.add('display-block')
    getS('.edit-block').classList.remove('display-block');
}
// end of function

//function which generates colors for text
getS('.btn-text-color').onclick = function () {
    for (let i = 0; i < getS('.colors').children.length; i++) {
        getS('.colors').children[i].style.backgroundColor = colors[i];
        getS('.colors').children[i].onclick = textColor;
    }
    getS('.colors').classList.add('display-flex');
}
// end of function

//function which generates color for background
getS('.btn-bg-color').onclick = function () {
    for (let i = 0; i < getS('.colors').children.length; i++) {
        getS('.colors').children[i].style.backgroundColor = colors[i];
        getS('.colors').children[i].onclick = bgColor;
    }
    getS('.colors').classList.add('display-flex');
}
// end of function

//function for check-box to make bold style of text
getS('#bold').onclick = function () {
    if (event.target.checked) {
        getS('.top-block').classList.add('bold');
    } else {
        getS('.top-block').classList.remove('bold');
    }
}
// end of function

//function for check-box to make italick style of text
getS('#italic').onclick = function () {
    if (event.target.checked) {
        getS('.top-block').classList.add('italic');
    } else {
        getS('.top-block').classList.remove('italic');
    }
}
// end of function

// function for button which opens block for creating table and list
getS('.btn-add').onclick = function () {
    getS('.first').classList.remove('display-block');
    getS('.second').classList.add('display-block');
}
// end of function

//function which generates list
getS('.btn-create-list').onclick = function () {
    const countList = getS('.count').value;
    const typeList = getS('.type').value;
    getS('.edit-area').value += `<ul type="${typeList}" style = "margin-left: 20px;">`;
    for (let i = 0; i < countList; i++) {
        getS('.edit-area').value += `<li>item ${i + 1} </li>`;
    }
    getS('.edit-area').value += '</ul>';
    getS('.first').classList.add('display-block');
    getS('.second').classList.remove('display-block');
}
// end of function


//function which open block .criate-list
getS('#list').onclick = function () {
    if (getS('#list').checked) {
        getS('.criate-table').classList.remove('display-block')
        getS('.criate-list').classList.add('display-block')
    }
}

getS('#table').onclick = function () {
    if (getS('#table').checked) {
        getS('.criate-list').classList.remove('display-block')
        getS('.criate-table').classList.add('display-block')
    }
}
// end of function

//function to create a table
getS('#create-table').onclick = function () {
    let count_tr = getS('#countTR').value
    let count_td = getS('#countTD').value
    let width_td = getS('#widthTD').value
    let height_td = getS('#heightTD').value
    let width_bord = getS('#width-border').value
    let tape_bord = getS('#type-border').value
    let collor_bord = getS('#color-border').value

    getS('.edit-area').value += `<table>`;
    for (let i = 0; i < count_tr; i++) {
        getS('.edit-area').value += `<tr>`;
        for (let i = 0; i < count_td; i++) {
            getS('.edit-area').value += `<td>TD</td>`;
        }
        getS('.edit-area').value += `<tr>`;
    }
    getS('.edit-area').value += `</table>`;

    getS('.criate-table').classList.remove('display-block')
    getS('.second').classList.remove('display-block')
    getS('.first').classList.add('display-block')
    document.head.innerHTML += `
<style>
table, td {
    border: ${width_bord}px ${tape_bord} ${collor_bord};
    border-collapse: collapse;
}
td {
    width: ${width_td}px;
    height: ${height_td}px;
}
</style>
`
}
// end of function

// function which changes font size
function fontSize() {
    getS('.top-block').style.fontSize = event.target.value;
}
// end of function

//function for changes font family
function fontFamily() {
    document.getElementsByTagName('option')[0].disabled = true;
    getS('.top-block').style.fontFamily = event.target.value;
}
// end of function

//function which changes text color
function textColor() {
    getS('.top-block').style.color = event.target.style.backgroundColor;
    getS('.colors').classList.remove('display-flex');
}
// end of function

//function which changes background color
function bgColor() {
    getS('.top-block').style.backgroundColor = event.target.style.backgroundColor;
    getS('.colors').classList.remove('display-flex');
}
// end of function

